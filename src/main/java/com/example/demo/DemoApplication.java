package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Arrays;
@SpringBootApplication
public class DemoApplication {


        public int  findMax(int [] arr) {
            int len = arr.length;
            int hlen=len/2;
            int i;
            int maxflag = 0;
            int temp;
            int j = 0;

            while (j<hlen ) {
                maxflag = 0;
                i=0;
                //查找最大值
                while (i<hlen-j) {
                    if (arr[maxflag] <arr[i]) {
                        maxflag = i;
                    }
                    i++;
                    // System.out.println(i);
                }
                //交换
                temp = arr[maxflag];
                arr[maxflag] = arr[hlen-j-1];
                arr[hlen-j-1] = temp;

                j++;




                // System.out.println(Arrays.toString(arr));
            }

            while (j>=hlen&&j<len  ) {
                maxflag = hlen +j-hlen ;
                i=hlen +j-hlen ;
                //查找最大值
                while (i<len ) {
                    if (arr[maxflag] <arr[i]) {
                        maxflag = i;
                    }
                    i++;
                    // System.out.println(i);
                }
                //交换
                temp = arr[maxflag];
                arr[maxflag] = arr[j];
                arr[j] = temp;

                j++;

                //System.out.println(Arrays.toString(arr));
            }

            return maxflag;
        }

    public static void main(String[] args) {
        int[] a = {1,2,3,4,5,6,7,8,9,10};
        int len = a.length;
        int maxflag = 0;
        System.out.println("原始数组:" +Arrays.toString(a));
        DemoApplication paixu = new DemoApplication();
        maxflag = paixu.findMax(a);
        System.out.println("从小到大和从大到小排序后数组:" +Arrays.toString(a));
        //System.out.println("从小到大素编号:" +Arrays.toString(a));
        SpringApplication.run(DemoApplication.class, args);
    }

}
